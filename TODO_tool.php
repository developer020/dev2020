<?php

$match_array = [];

scan_todo(__DIR__, $match_array);

if(count($match_array) > 0)
{
    echo implode("\r\n", $match_array)."\r\n";
}

/**
 * scans $directory recursively for files containing the keyword "TODO" (case insensitive) and stores results in $match_array
 * 
 * @param string $directory
 * @param array $match_array
 */
function scan_todo($directory, &$match_array)
{
    $dir_array = scandir($directory);

    if($dir_array === false)
    {
        return;
    }

    foreach($dir_array AS $name)
    {
        if(in_array($name, array(".", "..")))
        {
            continue;
        }
        
        $full_name = $directory.DIRECTORY_SEPARATOR.$name;
        
        if($full_name == __FILE__)
        {
            continue;
        }
        
        if(is_dir($full_name))
        {
            scan_todo($full_name, $match_array);
        }
        
        $file_content = file_get_contents($full_name);
        
        if($file_content === false)
        {
            continue;
        }
        
        if(stripos($file_content, "TODO") !== false)
        {
            $match_array[] = $full_name;
        }
    }
}


