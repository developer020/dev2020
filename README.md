Usage:

Place TODO_tool.php into the directory to be scanned for files containing the keyword "TODO" and execute the script by "php TODO_tool.php"

The script will output a list of all files in the same directory and recursivley subdirectories which contain "TODO" (case insensitive).